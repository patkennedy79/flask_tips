## Overview

This Flask application is used for generating tips for the @testdrivenio Twitter account.

This project is developed as part of the "Tips and Tricks" section on [testdriven.io](https://testdriven.io/courses/learn-flask/):

* [https://testdriven.io/tips/](https://testdriven.io/tips/)

## Installation Instructions

### Installation

Pull down the source code from this GitLab repository:

```sh
git clone git@gitlab.com:patkennedy79/flask_tips.git
```

Create a new virtual environment:

```sh
$ cd flask_tips
$ python3 -m venv venv
```

Activate the virtual environment:

```sh
$ source venv/bin/activate
```

Install the python packages specified in requirements.txt:

```sh
(venv) $ pip install -r requirements.txt
```

### Database Initialization

This Flask application needs a SQLite database to store data.  The database should be initialized via the Flask shell:

```
(venv) $ flask shell
>>> from project import database
>>> database.drop_all()
>>> database.create_all()
>>> quit()
(venv) $
```

### Running the Flask Application

Set the file that contains the Flask application and specify that the development environment should be used:

```sh
(venv) $ export FLASK_APP=app.py
(venv) $ export FLASK_ENV=development
```

Run development server to serve the Flask application:

```sh
(venv) $ flask run
```

Navigate to 'http://127.0.0.1:5000/' in your favorite web browser to view the website!

## Key Python Modules Used

* **Flask**: micro-framework for web application development which includes the following dependencies:
  * click: package for creating command-line interfaces (CLI)
  * itsdangerous: cryptographically sign data 
  * Jinja2: templating engine
  * MarkupSafe: escapes characters so text is safe to use in HTML and XML
  * Werkzeug: set of utilities for creating a Python application that can talk to a WSGI server
* **pytest**: framework for testing Python projects
* **flake8**: static analysis tool
* **pytest-cov**: pytest extension for running coverage.py to check code coverage of tests
* **Flask-SQLAlchemy**: ORM (Object Relational Mapper) for Flask

This application is written using Python 3.10.1.

## Testing

To run all the tests:

```sh
(venv) $ python -m pytest -v
```

To check the code coverage of the tests:

```sh
(venv) $ python -m pytest --cov-report term-missing --cov=project
```

NOTE: If working on Windows, use `set` instead of `export`.
