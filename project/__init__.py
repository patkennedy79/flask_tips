import click
from flask import Flask, current_app, render_template


# --------
# Database
# --------
database = ['Hello']


# ----------------------------
# Application Factory Function
# ----------------------------
def create_app():
    """Returns the Flask application."""

    # Create the Flask application
    app = Flask(__name__)

    register_blueprints(app)
    register_shell_context(app)
    register_cli_commands(app)
    register_app_routes(app)
    return app


# ----------------
# Helper Functions
# ----------------
def register_blueprints(app):
    """Register the Blueprints with the Flask application."""

    # Import the blueprints
    from project.users import users_blueprint

    # Since the application instance is now created, register each Blueprint
    # with the Flask application instance (app)
    app.register_blueprint(users_blueprint, url_prefix='/users')


def register_shell_context(app):
    """Add objects to the Flask shell context."""
    @app.shell_context_processor
    def shell_context():
        return {'database': database}


def register_cli_commands(app):
    """Register the CLI commands associated with the Flask application."""
    @app.cli.command('init_db')
    def initialize_database():
        """Initialize the SQLite database."""
        database.clear()
        click.echo('Initialized the SQLite database!')


def register_app_routes(app):
    """Register the routes associated with the Flask application."""

    @app.route('/')
    def index():
        return '<h1>Welcome to the Flask App!</h1>'

    @app.route('/logo')
    def flask_logo():
        return current_app.send_static_file('flask-logo.png')


def register_error_pages(app):
    @app.errorhandler(404)
    def page_not_found(e):
        return render_template('404.html'), 404
