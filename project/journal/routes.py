from flask import request, current_app
from werkzeug.utils import secure_filename

import os


@journal_blueprint.route('/journal', methods=['GET'])
def get_journal():
    # Only support secure protocols (HTTPS or WSS)
    if request.is_secure:
        current_app.logger.info(f'Journal request using protocol: {request.scheme}')
        return '<p>Journal Entries</p>'



@journal_blueprint.route('/<int:index>', methods=['PUT'])
def update_journal_entry(index):
    if request.method == 'PUT':
        # Update the journal entry in the database
        entry = Entry.query.filter_by(id=id).first_or_404()
        entry.update(request.form['entry_title'],
                     request.form['entry_text'])
        return '<p>User logged in!</p>'

    return '<h2>Login Form</h2>'


@journal_blueprint.route('/upload_file', methods=['POST'])
def upload_file():
    if 'file' in request.files:
        file = request.files['file']
        filename = secure_filename(file.filename)
        file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
        return '<p>File uploaded!</p>'

