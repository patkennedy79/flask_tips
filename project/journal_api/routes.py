from flask import abort

from . import journal_api_blueprint


# -------------
# API Endpoints
# -------------

@journal_api_blueprint.route('/<int:index>', methods=['GET'])
def get_journal_entry(index):
    """Return a journal entry"""
    user = token_auth.current_user()
    entry = Entry.query.filter_by(id=index).first_or_404()

    # Check that the journal entry is associated
    # with the current user
    if entry.user_id != user.id:
        abort(403)
    return entry


