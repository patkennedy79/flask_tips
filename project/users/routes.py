import click
from flask import request, current_app
from urllib.parse import urlparse

from . import users_blueprint


# ------------
# CLI Commands
# ------------

@users_blueprint.cli.command('create')
@click.argument('email')
def create(email):
    """Create a new user."""
    click.echo(f'Added new user (email: {email}!')


# ------
# Routes
# ------

@users_blueprint.route('/')
def get_users():
    return '<h1>List of Users</h1>'


@users_blueprint.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        # Validate request
        return '<p>New user registered!</p>'

    return '<h2>Registration Form</h2>'


@users_blueprint.route('/register', methods=['POST'])
def register():
    ...
    return '<h2>New User registered!</h2>', 201


@users_blueprint.route('/<id>')
def get_user(id):
    return f'<h2>Data for user #{id}</h2>'


@users_blueprint.route('/login')
def login():
    if request.method == 'POST':
        # Log in the user
        current_app.logger.info(f'New login request from from IP address: {request.remote_addr}')

        # Redirect the user to the specified URL after login
        if 'next' in request.args:
            next_url = request.args.get('next')

            # Only accept relative URLs
            if urlparse(next_url).scheme != '' or urlparse(next_url).netloc != '':
                current_app.logger.info(f'Invalid next path in login request: {next_url}')
                return abort(400)

            current_app.logger.info(f'Redirecting after valid login to: {next_url}')
            return redirect(next_url)

        return '<p>User logged in!</p>'

    return '<h2>Login Form</h2>'
