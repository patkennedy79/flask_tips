from flask import request

from . import users_api_blueprint


# -------------
# API Endpoints
# -------------

@users_api_blueprint.route('/register', methods=['POST'])
def register():
    # Return 201 (Created) to indicate the new user was created
    return '<h2>New User registered!</h2>', 201


@users_api_blueprint.route('/account', methods=['PUT'])
    if stock.user_id != current_user.id:
        abort(403)
