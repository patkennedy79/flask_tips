"""
This file (test_users.py) contains the functional tests for the 'users' blueprint.
"""


def test_index_page(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/')
    assert response.status_code == 200
    assert b'Welcome to the Flask App!' in response.data
